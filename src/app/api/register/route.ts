import prisma from "@/app/libs/prismadb";
import bcrypt from "bcrypt";
import { NextResponse } from "next/server";
export async function POST(request: Request) {
  const body = await request.json();
  const { email, name, password } = body;
  const userExist = await prisma.user.findUnique({ where: { email } });
  if (userExist)
    return NextResponse.json({ status: false, message: "Email already exits" });
  const hashedPassword = await bcrypt.hash(password, 12);
  const user = await prisma.user.create({
    data: { email, name, hashedPassword },
  });
  return NextResponse.json({
    status: true,
    message: "Register new account successful",
  });
}
