import React from "react";
import ClientOnly from "../components/ClientOnly";
import FavoriteClient from "./FavoriteClient";
import getCurrentUser from "../actions/getCurrentUser";
import EmptyState from "../components/EmptyState";
import getFavoriteListing from "../actions/getFavoriteListing";

const FavoritePage = async () => {
  const currentUser = await getCurrentUser();
  if (!currentUser) {
    return (
      <ClientOnly>
        <EmptyState title="Unauthorized" subtitle="Please login" />
      </ClientOnly>
    );
  }
  const listings = await getFavoriteListing();
  if (listings.length === 0) {
    return (
      <ClientOnly>
        <EmptyState
          title="No trips found"
          subtitle="Looks like you don't have any favorite place"
        />
      </ClientOnly>
    );
  }
  return (
    <ClientOnly>
      <FavoriteClient currentUser={currentUser} listings={listings} />
    </ClientOnly>
  );
};

export default FavoritePage;
