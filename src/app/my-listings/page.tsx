import React from "react";
import getCurrentUser from "../actions/getCurrentUser";
import ClientOnly from "../components/ClientOnly";
import EmptyState from "../components/EmptyState";
import getListings from "../actions/getListings";
import MyListingClient from "./MyListingClient";

const MyListingPage = async () => {
  const currentUser = await getCurrentUser();

  if (!currentUser) {
    return (
      <ClientOnly>
        <EmptyState title="Unauthorized" subtitle="Please login" />
      </ClientOnly>
    );
  }
  const listings = await getListings({ userId: currentUser.id });
  if (listings.length === 0) {
    return (
      <ClientOnly>
        <EmptyState
          title="No listing found"
          subtitle="Looks like you don't have any listing"
        />
      </ClientOnly>
    );
  }

  return <MyListingClient listings={listings} currentUser={currentUser} />;
};

export default MyListingPage;
