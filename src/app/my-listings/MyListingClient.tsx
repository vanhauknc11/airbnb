"use client";
import { Listing, User } from "@prisma/client";
import React, { useState } from "react";
import ListingCard from "../components/listings/ListingCard";
import Container from "../components/Container";
import axios from "axios";
import { toast } from "react-hot-toast";
import { useRouter } from "next/navigation";

interface MyListingClientProps {
  listings: Listing[];
  currentUser: User;
}

const MyListingClient: React.FC<MyListingClientProps> = ({
  listings,
  currentUser,
}) => {
  const [isLoading, setIsLoading] = useState(false);
  const router = useRouter();
  const onDeleteListing = (listingId: string) => {
    setIsLoading(true);
    axios
      .delete(`/api/listings/${listingId}`)
      .then(() => {
        toast.success("Delete Listing Successful !");
        router.refresh();
      })
      .catch((error: any) => {
        toast.error("Something Wrong!!!");
        console.log(error);
      })
      .finally(() => {
        setIsLoading(false);
      });
  };

  return (
    <Container>
      <div className="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4 xl:grid-cols-5 2xl:grid-cols-6 gap-8">
        {listings.map((listing) => (
          <ListingCard
            disabled={isLoading}
            data={listing}
            key={listing.id}
            currentUser={currentUser}
            actionLabel="Delete Listing"
            onAction={() => {
              onDeleteListing(listing.id);
            }}
          />
        ))}
      </div>
    </Container>
  );
};

export default MyListingClient;
